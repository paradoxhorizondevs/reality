import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  getHello() {
    this.http.get('https://finance-main.onrender.com/api/test/public/hello')
      .subscribe((response) => {
        console.log(response)
      })
  }
}
