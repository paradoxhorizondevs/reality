import { Component } from '@angular/core';
import {BackendService} from "./backend.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'reality';

  constructor(private backend: BackendService) {

  }

  public ngOnInit(): void {
    this.backend.getHello()
  }
}
